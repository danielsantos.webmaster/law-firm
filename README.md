# LAW FIRM 

Desenvolvido com Wordpress

## Otimizações de performance:

* 1) Tema desenvolvido do zero com conteúdo gerenciável por painel personalizado de custom fields.
* 2) Site com responsividade 
* 3) Compressão Dupla de Imagens ( Compress JPG e Tiny PNG )
* 3) Compressão de HTML, CSS e Javascript com o plugin Autoptimize
* 4) Minificação de Arquivos para diminuir as requisições do servidor com o plugin Autoptimize
* 5) Aproveitamento do Cache do Navegador ( Leverage Browser Caching in WordPress ) feito no arquivo .htacess

Uma técnica que poderia ser aplicada para melhorar ainda mais o desempenho seria a de CSS sprites, juntar todos os ícones em um arquivo único, diminuindo assim as requisões e aumentando o desempenho.

O site está em um servidor compartilhado, então alguns recursos são limitados e também está utilizando o servidor APACHE, que também tem o desempenho menor em relação ao NGINX. O Desempenho e segurança 
também poderia ser melhorado colocando o site em uma CDN como o Cloudflare.

## Segurança:

* 1) Não utilização do usuário padrão admin
* 2) Mudança do nome do usuário onde é diferente do nome de exibição
* 3) Bloqueio de login para ataques de força bruta
* 4) Saída do usuário WP à força, caso passe 60 minutos
* 5) Aprovar novos usuários manualmente
* 6) Captcha na página de registro
* 7) Honeypot na página de registro
* 8) Alteração do prefixo de URL padrão do banco de dados do wordpress
* 9) Permissões de arquivos e diretórios do wordpress configurados para não deixar brechas
* 10) Removi a capacidade de pessoas para editar arquivos PHP através do painel WP
* 11) Impedir o acesso a readme.html, license.txt and wp-config-sample.php
* 12) Bloqueio do XMLRPC
* 13) Desabilitado o Pingback no XMLRPC
* 14) Bloquear o acesso ao arquivo debug.log
* 15) Mudança da Página de login padrão do WP
* 16) Captcha e Honeypot nos formulários
* 17) Utilização do plugin ALL in one migration para backups manuais

## SEO

* 1) Tag de Título e Meta description de páginas inseridas.
* 2) Tag de Título e Meta description de posts inseridas.
* 3) Mostrar posts nos resultados de pesquisas
* 4) Mostrar categorias nos resultados de pesquisas
* 5) Mostrar arquivos do autor nos resultados de pesquisa
* 6) Logotipo da empresa inserido para aparecer junto com a tag titulo e meta description
* 7) Frase chave adicionada a página principal


Desenvolvido por Daniel Santos
